const button = document.getElementById("toggle-bar");
button.addEventListener("click", changePrices);

function changePrices() {
    let monthlyPayment = document.getElementsByTagName("h3");
    let annuallyPayment = document.getElementsByTagName("h4");

    Array(monthlyPayment.length).fill(0).map((ele, index) => {
        if (monthlyPayment[index].style.display === "none") {
            monthlyPayment[index].style.display = "block";
            annuallyPayment[index].style.display = "none";
            button.style.justifyContent = "flex-end";
        } else {
            monthlyPayment[index].style.display = "none";
            annuallyPayment[index].style.display = "block";
            button.style.justifyContent = "flex-start";
        }
    })
}